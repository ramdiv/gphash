import numpy as np

# 1 is painted, 0 is blank

def empty_board(x, y):
    np.zeros(x, y)

def board_of_sol(f, dimx, dimy):
    b = empty_board(dimx, dimy)
    for l in f:
        if l[0] == "PAINTSQ":
            [instr, r, c, s] = l
            for x in range(r - (2 * s + 1), r + (2 * s + 1) + 1):
                for y in range(c - (2 * s + 1), c + (2 * s + 1) + 1):
                    b[x][y] = 1
        elif l[0] == "ERASECELL":
            [instr, r, c] = l
            b[r][c] = 0
        else:
            assert false

def print_board(b):
    i = []
    for l in b:
        i.append(map(lambda x: "." if x == 0 else "#", l))
    return b
