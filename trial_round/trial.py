import numpy as np
import random

def output(sol, fn):
	f = open(fn, "w")
	f.write("{}\n".format(len(sol)))
	for r in sol:
		if r[0] == "PAINTSQ":
			f.write("{} {} {} {}\n".format(*r))
		elif r[0] == "ERASECELL":
			f.write("{} {} {}\n".format(*r))
	f.close()

def test(sol, n, m, orig):
	b = np.zeros([n, m])
	for r in sol:
		if r[0] == "PAINTSQ":
			cx, cz, dim = r[1:]
			b[cx-dim:cx+dim+1, cz-dim:cz+dim+1] = 1
		elif r[0] == "ERASECELL":
			r, c = r[1:]
			b[r, c] = 0
	print zip(*np.nonzero(orig-b))
	#print b.astype(int)
	#print orig

def eraser(sol, n, m, orig):
	b = np.zeros([n, m])
	for r in sol:
		if r[0] == "PAINTSQ":
			cx, cz, dim = r[1:]
			b[cx-dim:cx+dim+1, cz-dim:cz+dim+1] = 1
		elif r[0] == "ERASECELL":
			r, c = r[1:]
			b[r, c] = 0

	solution = []
	diff = orig - b
	for r in range(n):
		for c in range(m):
			if not diff[r, c] == 0:
				solution.append(("ERASECELL", r, c))
	return solution

def full_sol(board):
	solution = []
	N, M = board.shape
	for row in range(N):
		for col in range(M):
			if board[row, col] == 1:
				solution.append(["PAINTSQ", row, col, 0])
	return (solution, set(), len(solution))

def join(c1, c2):
	r1, c1, d1 = c1[1:]
	r2, c2, d2 = c2[1:]
	mx, Mx = min(r1 - d1, r2 - d2), max(r1 + d1, r2 + d2)
	my, My = min(c1 - d1, c2 - d2), max(c1 + d1, c2 + d2)
	dp = max(Mx - mx, My - my)
	d = dp / 2 + 1
	return ["PAINTSQ", mx+d, my+d, d] 

def diff(orig, com):
	r, c, d = com[1:]
	diff = np.copy(orig[r - d:r + d + 1, c - d:c + d + 1])
	diff -= np.ones(diff.shape)
	return map(lambda (rp, cp): ("ERASECELL", r - d + rp, c - d + cp), zip(*np.nonzero(diff)))

def incl(c, rep):
	r, c, d = c[1:]
	nr, nc, nd = rep[1:]
	if (r - d) >= (nr - nd) and (r + d + 1) <= (nr + nd + 1) and \
		(c - d) >= (nc - nd) and (c + d + 1) <= (nc + nd + 1): return True
	else: return False

def commit(orig, sol, c1, c2):
	prints, erasures, l = sol
	#c1, c2 = prints[i], prints[j]
	j = join(c1, c2)
	e = diff(orig, j)
	newpr, newer = filter(lambda c: not incl(c, j), prints) + [j], erasures | set(e)
	return (newpr, newer, len(newpr) + len(newer))

def distance(c1, c2):
	r1, c1, d1 = c1[1:]
	r2, c2, d2 = c2[1:]
	if abs(r1-r2) == abs(c1 - c2): return (abs(r1-r2) + abs(c1-c2))/2
	return (abs(r1-r2) + abs(c1-c2))

def run_test(orig, sol, seedc, curr = 0):
	def cmp(c1): 
		return distance(seedc, c1)
	while True:
		prints, erasures, l1 = sol
		print curr, l1
		c2 = min(prints, key = cmp)
		sol2 = commit(orig, sol, seedc, c2)
		prints, erasures, l2 = sol2
		if not len(prints): return sol
		seedc = random.choice(prints)
		curr += 1
		if l2 >= l1 and curr > 50000:
			return sol
		elif l2 < l1:
			sol = sol2
		else:
			seedc = random.choice(sol[0])



f = open("doodle.txt", "r")
contents = list(filter(lambda l: len(l) > 0, f.read().split("\n")))
board = list(map(lambda l: map(lambda x: 0 if x == "." else 1, l), contents[1:]))
board = np.array(board)
f.close()


orig = np.copy(board)

board = np.copy(orig)
solution = []
N, M = board.shape
for row in range(N):
	for col in range(M):
		if board[row, col] == 1:
			dim = 0
			#((2*dim+1)**2 - 2*dim - 1): dim += 1
			while board[row:(row+2*dim+1), col:(col+2*dim+1)].sum() >= ((2*dim+1)**2): dim += 1
			dim -= 1
			solution.append(["PAINTSQ", row+dim, col+dim, dim])
			board[row:(row+2*dim+1), col:(col+2*dim+1)] = 0#np.ones([2*dim+1, 2*dim+1])
er = eraser(solution, N, M, orig)
# 
# 	print n, len(solution)

sol = (solution, set(er), len(solution) + len(set(er)))
s, e, l = run_test(orig, sol, random.choice(solution))

solution = s + list(e)
test(solution, N, M, orig)

output(solution, "out.txt")