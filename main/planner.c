#include <string.h>
#include <stdlib.h>

#include "common.h"
#include "planner.h"

double planner(const struct problem *pb,
               uint32_t starting_point, int32_t remaining_time,
               uint8_t *visited, struct itinerary *out_path,
               uint32_t *out_length, uint32_t *out_cost,
               uint32_t lookaheads, uint32_t lookaheads_exact)
{
    struct itinerary *best_path = malloc(sizeof(struct itinerary));
    itinerary_init(best_path);


    /* Default solution is an empty solution */
    float best_score = -1;
    uint32_t best_cost = remaining_time;
    uint32_t best_length = 0;
    uint8_t *best_visited = malloc(pb->nb_streets);
    memcpy(best_visited, visited, pb->nb_streets);

    /* Compute lookaheads */
    for (struct endpoint_list *it = pb->junctions[starting_point].neighbourhood; it != NULL; it = it->next)
    {
        uint32_t length, cost;
        uint32_t length2 = 0, cost2 = 0;
        int32_t remaining_time2;
        float score;
        uint8_t *visited2 = NULL;
        struct itinerary *in_path = NULL;

        cost = pb->streets[it->street_id].cost;
        remaining_time2 = remaining_time - cost;

        if (remaining_time2 < 0)
            continue;

        if (visited[it->street_id] > 1 && lookaheads > lookaheads_exact)
            continue;

        visited2 = malloc(pb->nb_streets);
        memcpy(visited2, visited, pb->nb_streets);
        in_path = malloc(sizeof(struct itinerary));


        if (visited[it->street_id] == 0)
            length = pb->streets[it->street_id].length;
        else
            length = 0;

        visited2[it->street_id] = 2;

        if (lookaheads > 0)
        {
            planner(pb, it->junction_id, remaining_time2, visited2, in_path, &length2, &cost2, lookaheads - 1, lookaheads_exact);
        }
        else
        {
            itinerary_init(in_path);
        }

        visited2[it->street_id] = 1;

        score = ((float) (length + length2)) / ((float) (cost + cost2));
        if (score > best_score)
        {
            if (length2 == 0) // Don't waste steps
            {
                itinerary_free(in_path);
                itinerary_init(in_path);
            }
            else
            {
                cost += cost2;
                length += length2;
            }

            /* Free best */
            free(best_visited);
            itinerary_free(best_path);
            free(best_path);

            itinerary_prepend(in_path, it->junction_id, it->street_id);

            best_score = score;
            best_path = in_path;
            best_length = length;
            best_cost = cost;
            best_visited = visited2;
        }
        else
        {
            itinerary_free(in_path);
            free(in_path);
            free(visited2);
        }
    }

    /* Copy the best score */
    *out_path = *best_path;
    free(best_path);
    memcpy(visited, best_visited, pb->nb_streets);
    *out_cost = best_cost;
    *out_length = best_length;
    /* Free stuff */
    free(best_visited);
    return best_score;
}


double planner_loop(const struct problem *pb,
               uint32_t starting_point,
               uint32_t ending_point,
               int32_t remaining_time,
               uint8_t *visited, struct itinerary *out_path,
               uint32_t *out_length, uint32_t *out_cost,
               uint32_t lookaheads, uint8_t initial_call)
{
    struct itinerary *best_path = malloc(sizeof(struct itinerary));
    itinerary_init(best_path);


    /* Default solution is an empty solution */
    float best_score = -1;
    uint32_t best_cost = 0;
    uint32_t best_length = 0;
    uint8_t *best_visited = malloc(pb->nb_streets);
    memcpy(best_visited, visited, pb->nb_streets);

    /* Compute lookaheads */
    for (struct endpoint_list *it = pb->junctions[starting_point].neighbourhood; it != NULL; it = it->next)
    {
        uint32_t length, cost;
        uint32_t length2 = 0, cost2 = 0;
        int32_t remaining_time2;
        float score;
        uint8_t *visited2 = NULL;
        struct itinerary *in_path = NULL;

        cost = pb->streets[it->street_id].cost;
        remaining_time2 = remaining_time - cost;

        if (remaining_time2 < 0)
            continue;

        if (lookaheads == 0 && it->junction_id != ending_point)
            continue;

        if (visited[it->street_id] == 1 && initial_call)
            continue;
        if (visited[it->street_id] > 1)
            continue;

        visited2 = malloc(pb->nb_streets);
        memcpy(visited2, visited, pb->nb_streets);
        in_path = malloc(sizeof(struct itinerary));


        if (visited[it->street_id] == 0)
            length = pb->streets[it->street_id].length;
        else
            length = 0;

        visited2[it->street_id] = 2;

        if (lookaheads > 0)
        {
            planner_loop(pb, it->junction_id, ending_point, remaining_time2, visited2, in_path, &length2, &cost2, lookaheads - 1, 0);
            score = ((float) (length + length2)) / ((float) (cost + cost2));
            /* if we've reached the endpoint, add it as a possibility (paths of length <= N instead of paths of length = N) */
            if (score < ((float) length) / ((float) cost) && it->junction_id == ending_point)
            {
                length2 = cost2 = 0;
                itinerary_free(in_path);
                itinerary_init(in_path);
                memcpy(visited2, visited, pb->nb_streets);
            }
            else
            {
                cost += cost2;
                length += length2;
            }
        }
        else
        {
            itinerary_init(in_path);
        }

        visited2[it->street_id] = 1;

        /* Only consider paths ending correctly */
        if ((in_path->length > 0 && in_path->last->junction_id != ending_point)
            || (in_path->length == 0 && it->junction_id != ending_point))
        {
            itinerary_free(in_path);
            free(in_path);
            free(visited2);
            continue;
        }

        score = ((float) length) / ((float) cost);
        if (score > best_score)
        {
            /* Free best */
            free(best_visited);
            itinerary_free(best_path);
            free(best_path);

            itinerary_prepend(in_path, it->junction_id, it->street_id);

            best_score = score;
            best_path = in_path;
            best_length = length;
            best_cost = cost;
            best_visited = visited2;
        }
        else
        {
            itinerary_free(in_path);
            free(in_path);
            free(visited2);
        }
    }

    /* Copy the best score */
    *out_path = *best_path;
    free(best_path);
    memcpy(visited, best_visited, pb->nb_streets);
    *out_cost = best_cost;
    *out_length = best_length;
    /* Free stuff */
    free(best_visited);
    return best_score;
}


double planner_non_cycles(const struct problem *pb,
               uint32_t starting_point, int32_t remaining_time,
               uint8_t *visited, const uint8_t *visited_junctions,
               struct itinerary *out_path,
               uint32_t *out_length, uint32_t *out_cost,
               uint32_t lookaheads)
{
    struct itinerary *best_path = malloc(sizeof(struct itinerary));
    itinerary_init(best_path);


    /* Default solution is an empty solution */
    float best_score = -1;
    uint32_t best_cost = remaining_time;
    uint32_t best_length = 0;
    uint8_t *best_visited = malloc(pb->nb_streets);
    memcpy(best_visited, visited, pb->nb_streets);

    /* Compute lookaheads */
    for (struct endpoint_list *it = pb->junctions[starting_point].neighbourhood; it != NULL; it = it->next)
    {
        uint32_t length, cost;
        uint32_t length2 = 0, cost2 = 0;
        int32_t remaining_time2;
        float score;
        uint8_t *visited2 = NULL;
        uint8_t *visited_junctions2 = NULL;
        struct itinerary *in_path = NULL;

        cost = pb->streets[it->street_id].cost;
        remaining_time2 = remaining_time - cost;

        if (remaining_time2 < 0)
            continue;

        if (visited_junctions[it->junction_id] == 1)
            continue;

        visited2 = malloc(pb->nb_streets);
        memcpy(visited2, visited, pb->nb_streets);
        in_path = malloc(sizeof(struct itinerary));

        visited_junctions2 = malloc(pb->nb_junctions);
        memcpy(visited_junctions2, visited_junctions, pb->nb_junctions);
        visited_junctions2[starting_point] = 1;

        if (visited[it->street_id] == 0)
            length = pb->streets[it->street_id].length;
        else
            length = 0;

        visited2[it->street_id] = 1;
        if (lookaheads > 0)
        {
            planner_non_cycles(pb, it->junction_id, remaining_time2, visited2, visited_junctions2, in_path, &length2, &cost2, lookaheads - 1);
        }
        else
        {
            itinerary_init(in_path);
        }

        free(visited_junctions2);

        score = ((float) (length + length2)) / ((float) (cost + cost2));
        if (score > best_score)
        {
            if (length2 == 0) // Don't waste steps
            {
                itinerary_free(in_path);
                itinerary_init(in_path);
            }
            else
            {
                cost += cost2;
                length += length2;
            }

            /* Free best */
            free(best_visited);
            itinerary_free(best_path);
            free(best_path);

            itinerary_prepend(in_path, it->junction_id, it->street_id);

            best_score = score;
            best_path = in_path;
            best_length = length;
            best_cost = cost;
            best_visited = visited2;
        }
        else
        {
            itinerary_free(in_path);
            free(in_path);
            free(visited2);
        }
    }

    /* Copy the best score */
    *out_path = *best_path;
    free(best_path);
    memcpy(visited, best_visited, pb->nb_streets);
    *out_cost = best_cost;
    *out_length = best_length;
    /* Free stuff */
    free(best_visited);
    return best_score;
}

