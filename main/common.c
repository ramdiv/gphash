#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "common.h"


void itinerary_init(struct itinerary *it)
{
    it->fst = NULL;
    it->last = NULL;
    it->length = 0;
}


void itinerary_concat(struct itinerary *it1, struct itinerary *it2)
{
    if (it2->fst == NULL)
        return;

    it1->last->next = it2->fst;
    it1->last = it2->last;

    it1->length += it2->length;

    it2->length = 0;
    it2->fst = it2->last = NULL;
}


void itinerary_prepend(struct itinerary *it, uint16_t jun_id, uint16_t street_id)
{
    struct endpoint_list *t = malloc(sizeof (struct endpoint_list));
    t->junction_id = jun_id;
    t->street_id = street_id;
    t->next = it->fst;
    it->fst = t;
    if (it->last == NULL)
        it->last = t;
    it->length += 1;
}


void itinerary_append(struct itinerary *it, uint16_t jun_id, uint16_t street_id)
{
    struct endpoint_list *t = malloc(sizeof (struct endpoint_list));
    t->junction_id = jun_id;
    t->street_id = street_id;
    t->next = NULL;
    if (it->fst == NULL)
        it->fst = t;
    if (it->last != NULL)
        it->last->next = t;
    it->last = t;
    it->length += 1;
}


void itinerary_insert_full(struct itinerary *it1, struct itinerary *it2, struct endpoint_list *pos)
{
    if (it2->fst == NULL)
        return;


    if (it2->last->junction_id != pos->junction_id)
        abort();

    it2->last->next = pos->next;

    if (it2->last->next == NULL)
        it1->last = it2->last;

    pos->next = it2->fst;

    it1->length += it2->length;


    it2->length = 0;
    it2->fst = it2->last = NULL;
}


void itinerary_free(struct itinerary *itinerary)
{
    struct endpoint_list *next = itinerary->fst;
    for (struct endpoint_list *it = next; next != NULL; it = next)
    {
        next = it->next;
        free(it);
    }
    itinerary->length = 0;
    itinerary->fst = itinerary->last = NULL;
}

struct state *parse_state(const struct problem *problem, const char *path)
{
    unsigned int nb_cars, nb_stops, previous_stop, next_stop;

    FILE *f = fopen(path, "r");

    struct state *state = malloc(sizeof(struct state));
    state->score = 0;
    state->visited = calloc(problem->nb_streets, 1);
    state->solution = malloc(sizeof(struct car) * problem->nb_cars);

    fscanf(f, "%u", &nb_cars);
    if (nb_cars != problem->nb_cars) abort();

    for (unsigned int i = 0; i < nb_cars; i++)
    {
        itinerary_init(&state->solution[i].path);
        previous_stop = problem->start_junction;
        state->solution[i].remaining_time = problem->allowed_time;
        itinerary_append(&state->solution[i].path, previous_stop, -1);
        fscanf(f, "%u", &nb_stops);
        for (unsigned int j = 0; j < nb_stops; j++)
        {
            fscanf(f, "%u", &next_stop);
            if (j == 0) continue;
            for (struct endpoint_list *it = problem->junctions[previous_stop].neighbourhood; it != NULL; it = it->next)
            {
                if (it->junction_id == next_stop)
                {
                    itinerary_append(&state->solution[i].path, next_stop, it->street_id);
                    state->solution[i].remaining_time -= problem->streets[it->street_id].cost;
                    if (state->visited[it->street_id] == 0)
                        state->score += problem->streets[it->street_id].length;
                    state->visited[it->street_id] = 1;
                    break;
                }
            }
            previous_stop = next_stop;
        }
        printf("Car %d: %d / %d stops\n", i, state->solution[i].path.length, nb_stops);
    }

    return state;
}


struct problem *read_problem(const char *path)
{
    struct problem *pb;
    unsigned int start, end, length, cost, direction;
    unsigned int nb_junctions, nb_streets, allowed_time, nb_cars, start_junction;
    float x, y;
    FILE *f = fopen(path, "r");
    if (f == NULL)
        return NULL;
    if (fscanf(f, "%u %u %u %u %u", &nb_junctions, &nb_streets, &allowed_time, &nb_cars, &start_junction) != 5) return NULL;

    pb = malloc(sizeof(struct problem));
    pb->nb_junctions = nb_junctions; pb->nb_streets = nb_streets;
    pb->allowed_time = allowed_time; pb->nb_cars = nb_cars;
    pb->start_junction = start_junction;

    /* Ignore it... */
    for (unsigned int i = 0; i < nb_junctions; i++)
        if (fscanf(f, "%f %f", &x, &y) != 2) return NULL;

    pb->streets = malloc(sizeof(struct street) * nb_streets);

    /* Streets */
    for (unsigned int i = 0; i < nb_streets; i++)
    {
        if (fscanf(f, "%u %u %u %u %u", &start, &end, &direction, &cost, &length) != 5) return NULL;
        pb->streets[i].start = start;
        pb->streets[i].end = end;
        pb->streets[i].direction = direction;
        pb->streets[i].cost = cost;
        pb->streets[i].length = length;
    }

    fclose(f);

    /* Build junction lists */
    pb->junctions = malloc(sizeof(struct junction) * nb_junctions);
    for (unsigned int i = 0; i < nb_junctions; i++)
    {
        struct endpoint_list *head = NULL;
        struct endpoint_list *old_head = NULL;
        for (unsigned int j = 0; j < nb_streets; j++)
        {
            if (pb->streets[j].start == i)
            {
                old_head = head;
                head = malloc(sizeof(struct endpoint_list));
                head->next = old_head;
                head->junction_id = pb->streets[j].end;
                head->street_id = j;
            }
            if (pb->streets[j].direction == 2 && pb->streets[j].end == i)
            {
                old_head = head;
                head = malloc(sizeof(struct endpoint_list));
                head->next = old_head;
                head->junction_id = pb->streets[j].start;
                head->street_id = j;
            }
        }
        pb->junctions[i].neighbourhood = head;
    }

    return pb;
}


void problem_free(struct problem *problem)
{
    free(problem->streets);
    free(problem->junctions);
    free(problem);
}


void state_free(struct problem *problem, struct state *state)
{
    for (int i=0; i < problem->nb_cars; i++)
    {
        itinerary_free(&state->solution[i].path);
    }
    free(state->solution);
    free(state->visited);
    free(state);
}


struct state *init_state(const struct problem *problem)
{
    struct state *state = malloc(sizeof(struct state));
    state->score = 0;
    state->visited = calloc(problem->nb_streets, 1);
    state->solution = malloc(sizeof(struct car) * problem->nb_cars);

    for (unsigned int i = 0; i < problem->nb_cars; i++)
    {
        itinerary_init(&state->solution[i].path);
        itinerary_append(&state->solution[i].path, problem->start_junction, -1);
        state->solution[i].remaining_time = problem->allowed_time;
    }

    return state;
}


void output_result(const struct problem *pb, const struct state *state, const char *path)
{
    FILE *f = fopen(path, "w");
    fprintf(f, "%u\n", pb->nb_cars);
    for (unsigned int i=0; i < pb->nb_cars; i++)
    {
        fprintf(f, "%u\n", state->solution[i].path.length);
        for (struct endpoint_list *it = state->solution[i].path.fst; it != NULL; it = it->next)
            fprintf(f, "%u\n", it->junction_id);
    }
    fclose(f);
}

