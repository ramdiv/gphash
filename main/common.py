from collections import namedtuple

DIRECTION_ONE, DIRECTION_BOTH = 1, 2

street_t = namedtuple('Street', ['start', 'end', 'direction', 'cost', 'length', 'visited'])
junction_t = namedtuple('Junction', ['coords', 'streets_map'])
problem_t = namedtuple('Problem', ['junctions', 'streets', 'time', 'cars', 'start'])

def decode_file(f):
    nb_junctions, nb_streets, time, c, start = tuple(int(x) for x in f.readline().strip('\n').split(' '))
    junctions = [None] * nb_junctions
    streets = [None] * nb_streets
    for i in xrange(nb_junctions):
        coords = tuple(float(x) for x in f.readline().strip('\n').split(' '))
        junctions[i] = junction_t(coords, dict())
    for i in xrange(nb_streets):
        s, e, d, t, l = tuple(int(x) for x in f.readline().strip('\n').split(' '))
        streets[i] = street_t(s, e, d, t, l, [])
        junctions[s][1][e] = i
        if d == DIRECTION_BOTH:
            junctions[e][1][s] = i
    return problem_t(junctions, streets, time, c, start)


def get_street_from_junctions(problem, start, end):
    streets = problem.junctions[start].streets_map
    if end in streets:
        return streets[end]
    else:
        return None
