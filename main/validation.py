from common import *
from copy import deepcopy

def parse_output_file(fn):
    """return a list containing a list of junctions visited for every car"""
    f = open(fn, "r")
    nb_cars = int(f.readline())
    cars = []
    for car in range(nb_cars):
        car = []
        nb_junctions = int(f.readline())
        for j in range(nb_junctions):
            car.append(int(f.readline()))
        cars.append(car)
    return cars

def distance_of_junctions(problem, junctions_visited):
    # [0, 1, 2]
    if len(junctions_visited) == 0:
        return 0
    def aux(mytuple, junc):
        acc_dist, prev_j = mytuple
        num_street = get_street_from_junctions(problem, prev_j, junc)
        if not problem.streets[num_street].visited:
            acc_dist = acc_dist + problem.streets[num_street].length
        problem.streets[num_street].visited.append(1)
        return (acc_dist, junc)
    (d, end) = (reduce(aux, junctions_visited[1:], (0, junctions_visited[0])))
    return d

def distance_of_solution(problem, sol):
    problem = deepcopy(problem)
    d = 0.0
    for car in sol:
        d += distance_of_junctions(problem, car)
    return d


def is_valid(problem, sol):
    problem = deepcopy(problem)
    sol = deepcopy(sol)
    for car in sol:
        start = car.pop(0)
        if problem.start != start:
            return False
        credit = problem.time
        for x in car:
            street = get_street_from_junctions(problem, start, x)
            if street is None:
                return False
            street_data = problem.streets[street]
            credit -= street_data.cost
            if credit < 0:
                return False
            start = x
    return True


def test_dist_of_junctions():
    with open('paris_54000.txt', 'r') as file:
        problem = decode_file(file)
    print distance_of_junctions(problem, [10659, 6851])


if __name__ == '__main__':
    import sys
    with open('paris_54000.txt', 'r') as file:
        problem = decode_file(file)
    sol = parse_output_file(sys.argv[1])
    assert (is_valid(problem, sol))
    print(distance_of_solution(problem, sol))

