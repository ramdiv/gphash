import common
import bfs
import output
import validation
import random
from copy import deepcopy

with open('paris_54000.txt', 'r') as file:
    problem = common.decode_file(file)

random.seed()
result = bfs.run_random(deepcopy(problem))
print(validation.distance_of_solution(problem, result))
assert (validation.is_valid(problem, result))
output.output_solution(result, "out.txt")
