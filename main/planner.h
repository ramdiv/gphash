#ifndef __PLANNER42__H__
#define __PLANNER42__H__

#include "common.h"

double planner(const struct problem *pb,
               uint32_t starting_point, int32_t remaining_time,
               uint8_t *visited, struct itinerary *out_path,
               uint32_t *out_length, uint32_t *out_cost,
               uint32_t lookaheads, uint32_t lookaheads_exact);


double planner_loop(const struct problem *pb,
               uint32_t starting_point,
               uint32_t ending_point,
               int32_t remaining_time,
               uint8_t *visited, struct itinerary *out_path,
               uint32_t *out_length, uint32_t *out_cost,
               uint32_t lookaheads, uint8_t initial_call);


double planner_non_cycles(const struct problem *pb,
               uint32_t starting_point, int32_t remaining_time,
               uint8_t *visited, const uint8_t *visited_junctions,
               struct itinerary *out_path,
               uint32_t *out_length, uint32_t *out_cost,
               uint32_t lookaheads);

#endif
