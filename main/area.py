from common import *

import math

def distance_on_earth(lat1, long1, lat2, long2):
    if (lat1, long1) == (lat2, long2): return 0.
    degrees_to_radians = math.pi/180.0
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians
    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) +
           math.cos(phi1)*math.cos(phi2))
    arc = math.acos( cos )
    return arc * 6371009


def sort_streets_by_speed(problem, streets):
    def cmp_speed (ind_x, ind_y):
        x = problem.streets[ind_x]
        y = problem.streets[ind_y]
        return (x.length / x.cost) - (y.length / y.cost)
    return sorted(streets, cmp = cmp_speed)

def junctions_of_area(problem, x, y, radius):
    js = []
    for ind_j in range(len(problem.junctions)):
        jx, jy = problem.junctions[ind_j].coords
        if distance_on_earth(x, y, jx, jy) <= radius:
            js.append(ind_j)
    return js

def open_streets_of_area(problem, x, y, radius):
    # find all the junctions in the area
    # return a _set_ of their streets
    streets = set()
    for j in junctions_of_area(problem, x, y, radius):
        for s in problem.junctions[j].streets_map.values():
            if not problem.streets[s].visited:
                streets.add(s)
    return streets

def score_of_streets(problem, streets, time):
    d = 0
    streets = sort_streets_by_speed(problem, streets)
    for s_ind in streets:
        s = problem.streets[s_ind]
        if time < s.cost:
            continue
        d += s.length
        time -= s.cost
    return d

def score_of_area(problem, x, y, radius, time):
    streets = open_streets_of_area(problem, x, y, radius)
    return score_of_streets(problem, streets, time)
