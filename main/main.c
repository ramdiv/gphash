#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "common.h"
#include "planner.h"



int main(int argc, const char **argv)
{
    struct problem *problem = read_problem("paris_54000.txt");
    struct state *state = NULL;

    if (argc < 3)
    {
        fprintf(stderr, "Usage: %s DEPTH DEPTH_EXACT [partial-solution]\n");
        return EXIT_FAILURE;
    }

    printf("Problem loaded!\n");

    int MAGICCONSTANT = atoi(argv[1]);
    int MAGICCONSTANT2 = atoi(argv[2]);

    char file_name[4096];

    if (argc > 3)
    {
        state = parse_state(problem, argv[3]);
        printf("State file \"%s\" loaded!\n", argv[3]);
    }
    else
        state = init_state(problem);

    sprintf(file_name, "result-c-%03d-%03d-out.txt", MAGICCONSTANT, MAGICCONSTANT2);

    uint8_t *visited_junctions = calloc(problem->nb_junctions, 1);
    uint8_t *best_visited = calloc(problem->nb_streets, 1);
    uint8_t *visited2 = calloc(problem->nb_streets, 1);
    float best_score = 0;

    uint8_t going_on = 0;

    printf("Depth: %d, of which %d are to be exact. Computing...\n", MAGICCONSTANT, MAGICCONSTANT2);

    do
    {
        going_on = 0;
        for (unsigned int i=0; i < problem->nb_cars; i++)
        {
            if (state->solution[i].remaining_time > 0)
            {
                uint32_t best_time, best_distance;
                struct itinerary *best_it = malloc(sizeof(struct itinerary));
                struct endpoint_list *best_pos = NULL;
                itinerary_init(best_it);

                memcpy(best_visited, state->visited, problem->nb_streets);

                best_score = planner(problem, state->solution[i].path.last->junction_id,
                                state->solution[i].remaining_time, best_visited, best_it, &best_distance, &best_time, MAGICCONSTANT, MAGICCONSTANT2);

                for (struct endpoint_list *it = state->solution[i].path.fst; it->next != NULL; it = it->next)
                {
                    float score = 0;
                    uint32_t time2, distance2;
                    struct itinerary *itinerary = malloc(sizeof(struct itinerary));
                    memcpy(visited2, state->visited, problem->nb_streets);
                    itinerary_init(itinerary);
                    score = planner_loop(problem, it->junction_id, it->junction_id, state->solution[i].remaining_time, visited2, itinerary, &distance2, &time2, MAGICCONSTANT, 1);
                    if (score > best_score)
                    {
                        uint8_t *tmp;
                        tmp = best_visited;
                        best_visited = visited2;
                        visited2 = tmp;

                        best_pos = it;
                        best_time = time2;
                        best_distance = distance2;
                        itinerary_free(best_it);
                        free(best_it);

                        best_it = itinerary;
                        best_score = score;
                    }
                    else
                    {
                        itinerary_free(itinerary);
                        free(itinerary);
                    }
                }

                if (best_distance == 0)
                {
                    /* The car is stuck, we should do something fancy… */
                    /* TODO: this part is to be changed with something that moves the car to the nearest unexplored street */
                    printf("Car %d don't know where to go.\n", i);
                    state->solution[i].remaining_time = -(state->solution[i].remaining_time);
                }
                else
                {
                    uint8_t *tmp;
                    tmp = state->visited;
                    state->visited = best_visited;
                    best_visited = tmp;

                    state->solution[i].remaining_time -= best_time;
                    if (best_pos == NULL)
                        itinerary_concat(&(state->solution[i].path), best_it);
                    else
                    {
                        itinerary_insert_full(&(state->solution[i].path), best_it, best_pos);
                    }
                    state->score += best_distance;
                    going_on = 1;
                }

                itinerary_free(best_it);
                free(best_it);
            }
        }

        /* TODO: help stuck cars */
        for (unsigned int k=0; k < 15; k++)
        if (going_on == 0)
        {
            printf("Some cars are stuck... helping them...\n");
            for (unsigned int i=0; i < problem->nb_cars; i++)
            {
                if (state->solution[i].remaining_time < 0)
                {
                    /* TODO: change this to something locating the nearest accessible unexplored street */
                    uint32_t time, distance;
                    struct itinerary it;

                    state->solution[i].remaining_time = -(state->solution[i].remaining_time);

                    planner_non_cycles(problem, state->solution[i].path.last->junction_id,
                             state->solution[i].remaining_time, state->visited, visited_junctions, &it, &distance, &time, MAGICCONSTANT * (2 + k));

                    if (distance == 0)
                    {
                        /* The car is stuck, we should do something fancy… */
                        printf("Car %d don't know where to go (attempt %d).\n", i, k);
                        state->solution[i].remaining_time = -(state->solution[i].remaining_time);
                        itinerary_free(&it);
                    }
                    else
                    {
                        state->solution[i].remaining_time -= time;
                        itinerary_concat(&(state->solution[i].path), &it);
                        state->score += distance;
                        going_on = 1;
                    }
                }
            }
            printf("Finsihed helping!\n");
        }

        printf("Score so far: %07d\n", state->score);
        printf("Remaining time:");
        for (unsigned int i = 0; i < problem->nb_cars; i++)
            if (state->solution[i].remaining_time < 0)
                printf(" %d", - (state->solution[i].remaining_time));
            else
                printf(" %d", (state->solution[i].remaining_time));
        printf("\n");
        output_result(problem, state, file_name);
    }
    while(going_on);

    free(visited_junctions);

    printf("Score: %d\n", state->score);

    output_result(problem, state, "result-c.txt");

    state_free(problem, state);
    problem_free(problem);

    return 0;
}
