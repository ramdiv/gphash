import bfs
import common
import itertools
import validation
from copy import deepcopy
import output


max_score_so_far = 0

def record_best(c, score):
    global max_score_so_far
    if max_score_so_far < score:
        max_score_so_far = score
        output.output_solution([car[0] for car in c], "wip-nocut-no-penalty.txt")
        print(score)


def generate_possible_steps(problem, car):
    path, remaining_time = car

    pos = path[-1]
    for neighbour in bfs.neigh(problem.junctions, pos):
        street = problem.streets[common.get_street_from_junctions(problem, pos, neighbour)]
        if street.cost <= remaining_time:
            yield [path + [neighbour], remaining_time - street.cost]


def search_step_parallel(orig, problem, juncts, streets, state, scoref=bfs.max_speed, current_depth=0, min_depth=0, max_depth=50):
    print('Depth: %d' % current_depth)

    if current_depth < min_depth:
        bfs.search_step(problem, juncts, streets, state, scoref)
        return search_step_parallel(orig, problem, juncts, streets, state, scoref, current_depth + 1, min_depth, max_depth)

    if current_depth >= max_depth:
        while bfs.search_step(problem, juncts, streets, state, scoref): None
        return


    best_problem = deepcopy(problem)
    best_c = deepcopy(state)
    while bfs.search_step(best_problem, juncts, streets, best_c, scoref): None
    r = [car[0] for car in best_c]
    best_score = validation.distance_of_solution(orig, r)
    record_best(best_c, best_score)

    count = 0
    possible_continuations = itertools.product(*(generate_possible_steps(problem, car) for car in state))
    for c in possible_continuations:
        count += 1
        if count > 30:
            break
        problem2 = deepcopy(problem)
        search_step_parallel(orig, problem2, juncts, streets, c, scoref, current_depth + 1, min_depth, max_depth)
        # State is c.
        r = [car[0] for car in c]
        assert (validation.is_valid(problem2, r))
        score = validation.distance_of_solution(orig, r)
        assert (score > 0), c
        if best_score < score:
            best_score = score
            best_c = c
            best_problem = problem2
            record_best(best_c, best_score)


def run(problem):
    orig = deepcopy(problem)
    pr = bfs.init(problem)
    while search_step_parallel(orig, *pr, scoref = bfs.max_speed): None
    pr, j, s, state = pr
    return [car[0] for car in state]
