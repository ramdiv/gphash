from common import *
from copy import deepcopy
import validation
import math
import area
import random

def neigh(juncts, i):
    return juncts[i][1].keys()

def max_speed(streets, problem, i, j):
    street = streets[get_street_from_junctions(problem, i, j)]
    speed = float(street.length) / street.cost
    if len(street.visited) == 0: return speed
    else: return speed / ((len(street.visited) + 1.1) ** 10)

def min_cost(streets, problem, i, j):
    street = streets[get_street_from_junctions(problem, i, j)]
    cost = -street.cost
    if len(street.visited) == 0: return cost
    else: return cost * ((len(street.visited) + 1)**2)

def max_neigh(problem, juncts, streets, i, maxtime, scoref):
    def keyf(j): return scoref(streets, problem, i, j)
    def filtr(j):
        street = streets[get_street_from_junctions(problem, i, j)]
        return street.cost <= maxtime
    nei = filter(filtr, neigh(juncts, i))
    if not len(nei): return None
    return max(nei, key = keyf)

def max_neigh_wd(car, streets, problem, i, j):
#    def smapr((it, time)):
#        def ismapr(jun):
#            if jun == j: 0
#            x1, y1 = problem.junctions[it[-1]].coords
#            x2, y2 = problem.junctions[j].coords
#            return area.distance_on_earth(x1, y1, x2, y2)
#        return(max(map(ismapr, it[-5:])))
#    d = min(map(smapr, state))
    street = streets[get_street_from_junctions(problem, i, j)]
    speed = float(street.length) / street.cost
    if not (len(car[0])-1) % 100:
        x2, y2 = problem.junctions[j].coords
        return area.score_of_area(problem, x2, y2, 100, car[1])
    if len(street.visited) == 0: return speed
    else: return speed / ((len(street.visited) + 1.1) ** (min([len(car[0]), 20]) - 10))


    #return -d - len(street.visited)
    #return -(len(street.visited) ** 2)
#    return d #- (len(street.visited) * d) + speed

def search_step(problem, juncts, streets, state, scoref = max_speed):
    flag = False
    for (i, car) in enumerate(state):
        def mnwd(streets, problem, i, j):
            return max_neigh_wd(car, streets, problem, i, j)
        it, time = car
        nxt = max_neigh(problem, juncts, streets, it[-1], time, scoref)
        if not nxt == None:
            street = streets[get_street_from_junctions(problem, it[-1], nxt)]
            street.visited.append(i)
            car[0].append(nxt)
            car[1] -= street.cost
            #print car[1]
            flag = True
    print [car[1] for car in state]
    return flag

def search_step_random(problem, juncts, streets, state):
    flag = False
    for (cari, car) in enumerate(state):
        it, time = car
        i = it[-1]
        def filtrtime(j):
            street = streets[get_street_from_junctions(problem, i, j)]
            return street.cost <= car[1]
        def filtrvisited(j):
            street = streets[get_street_from_junctions(problem, i, j)]
            return len(street.visited) == 0
        nei = neigh(juncts, i)
        neif = filter(filtrtime, nei)
        if len(neif) > 0:
            neifv = filter(filtrvisited, neif)
            nxt = random.choice(neifv) if len(neifv) > 0 else random.choice(neif)
            street = streets[get_street_from_junctions(problem, i, nxt)]
            street.visited.append(cari)
            car[0].append(nxt)
            car[1] -= street.cost
            #print car[1]
            flag = True
    return flag

def init(problem):
    juncts = problem.junctions
    streets = problem.streets
    cars = [[[problem.start], problem.time] for i in range(problem.cars)]
    return (problem, juncts, streets, cars)

def search_seq(i, c, problem, juncts, streets, scoref = max_speed):
    car = [[i], c]
    flag = True
    while flag:
        flag = False
        it, time = car
        nxt = max_neigh(problem, juncts, streets, it[-1], time, scoref)
        if not nxt == None:
            street = streets[get_street_from_junctions(problem, it[-1], nxt)]
            street.visited.append(i)
            car[0].append(nxt)
            car[1] -= street.cost
            flag = True
    return car[0]

def run(problem):
    pr = init(problem)
    while search_step(*pr, scoref = max_speed): None
    pr, j, s, state = pr
    return [car[0] for car in state]

def run_seq(problem):
    pr, j, s, state = init(problem)
    return [search_seq(car[0][0], car[1], pr, j, s, scoref = max_speed) for car in state]

def run_random(problem):
    pr = init(problem)
    while search_step_random(*pr): None
    pr, j, s, state = pr
    return [car[0] for car in state]


def lookahead_seq(path, time, problem, scoresofar, curr, scoref = max_speed):
    streets, juncts = problem.streets, problem.junctions
    def filtr(j):
        i = path[-1]
        street = streets[get_street_from_junctions(problem, i, j)]
        return street.cost <= time
    def mappr(j):
        prb = deepcopy(problem)
        streets, juncts = prb.streets, prb.junctions
        i = path[-1]
        street = streets[get_street_from_junctions(problem, i, j)]
        part_path = search_seq(j, time - street.cost, prb, 
                               juncts, streets, scoref = scoref)

        return (part_path, time-street.cost, prb, 
                validation.distance_of_junctions(deepcopy(problem), part_path))
    def maxr(s): return s[-1]

    i = path[-1]
    n = neigh(juncts, i)
    n = filter(filtr, n)
    if not len(n): return (path, problem)
    if curr > 0:
        prb = deepcopy(problem)
        streets, juncts = prb.streets, prb.junctions
        i = path[-1]
        part_path = search_seq(i, time, prb, 
                               juncts, streets, scoref = scoref)
        return (prb, path + part_path[1:])

    possible = map(mappr, n)
    m = max(possible, key=maxr)
    print path, m[-1], time
    return lookahead_seq(path + [m[0][0]], m[1], m[2], m[-1], curr + 1,
                         scoref = scoref)
    
def run_lookahead_seq(problem):
    pr, j, s, state = init(problem)
    sol = []
    for car in state:
        print(len(sol))
        pr, path = lookahead_seq(car[0], car[1], pr, 0, 0, 
                                 scoref = max_speed)
        sol.append(path)
    return sol
