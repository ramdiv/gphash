import common
import bfs
import output
import validation
import random
from copy import deepcopy

with open('paris_54000.txt', 'r') as file:
    problem = common.decode_file(file)

random.seed()
result = bfs.run_random(deepcopy(problem))
d = validation.distance_of_solution(problem, result)
while True:
    result = bfs.run_random(deepcopy(problem))
    d_new = validation.distance_of_solution(problem, result)
    if d_new > d:
        output.output_solution(result, "out_" + str(d_new) + ".txt")
        print "increased from " + str(d) + " to " + str(d_new)
        d = d_new
