#ifndef __COMMON42__H__
#define __COMMON42__H__

#include <stdint.h>

struct street {
    uint16_t start, end, length, cost;
    uint8_t direction;
};

struct endpoint_list {
    uint16_t junction_id;
    uint16_t street_id;
    struct endpoint_list *next;
};

struct junction {
    struct endpoint_list *neighbourhood;
};

struct problem {
    uint16_t nb_junctions, nb_streets, allowed_time, nb_cars, start_junction;
    struct street *streets;
    struct junction *junctions;
};


struct itinerary {
    struct endpoint_list *fst, *last;
    uint32_t length;
};


struct car {
    struct itinerary path;
    int32_t remaining_time;
};


struct state {
    uint32_t score;
    uint8_t *visited;
    struct car *solution;
};


void itinerary_init(struct itinerary *it);
void itinerary_concat(struct itinerary *it1, struct itinerary *it2);
void itinerary_prepend(struct itinerary *it, uint16_t jun_id, uint16_t street_id);
void itinerary_append(struct itinerary *it, uint16_t jun_id, uint16_t street_id);

/* Plug it2 into it1 after pos (assuming it's part of it1). it2 is then invalidated */
void itinerary_insert_full(struct itinerary *it1, struct itinerary *it2, struct endpoint_list *pos);
void itinerary_free(struct itinerary *itinerary);


struct state *parse_state(const struct problem *problem, const char *path);
struct problem *read_problem(const char *path);


void problem_free(struct problem *problem);

struct state *init_state(const struct problem *problem);
void state_free(struct problem *problem, struct state *state);


void output_result(const struct problem *pb, const struct state *state, const char *path);

#endif
