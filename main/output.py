def output_solution(sol, fname):
    f = open(fname, "w")
    f.write("{}\n".format(len(sol)))
    for car in sol:
        f.write("{}\n".format(len(car)))
        for junc in car:
            f.write("{}\n".format(junc))


#test
#output_solution([[0],[0,1,2]], "out.txt")
